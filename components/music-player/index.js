// components/music-player/index.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    // 这里定义了name属性，属性值可以在组件使用时指定
    name: {
      type: String,
      value: '未知歌曲',
      observer (newVal) {
        // 通常 newVal 就是新设置的数据， oldVal 是旧数据
        this.setData({
          title: newVal
        })
     }
    },

    id: {
      type: String,
      value: ''
    },

    play: {
      type: Boolean,
      value: true,
      observer (newVal) {
        console.log('音乐组件外面传进来的播放状态', newVal)
        // 通常 newVal 就是新设置的数据， oldVal 是旧数据
        this.setData({
          isPlay: newVal
        })
        console.log(this.data.isPlay)
     }
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    title: '未知歌曲',
    isPlay: true
  },

  ready () {
    console.log('-------------------------在组件布局完成后执行，此时可以获取节点信息---------------------------')
    this.fetchMusic()
  },

  /**
   * 组件的方法列表
   */
  methods: {
    fetchMusic () {
      console.log(123333)
      const backgroundAudioManager = wx.getBackgroundAudioManager()
  
      console.log(backgroundAudioManager.src)
      if (!backgroundAudioManager.src) return
  
      // 获取本地存储
      backgroundAudioManager.onEnded(() => {
        console.log('播放自然介绍')
        // todo完成自动获取数据
        this.triggerEvent('playevent', {
          play: false
        })
      })

      backgroundAudioManager.onPause(() => {
        // 暂停
        console.log('播放暂停')
        this.triggerEvent('playevent', {
          play: false
        })
      })

      backgroundAudioManager.onStop(() => {
        // 停止
        console.log('播放停止')
        this.triggerEvent('playevent', {
          play: false
        })
      })

      backgroundAudioManager.onPlay(() => {
        // 开始
        console.log('播放开始')
        this.triggerEvent('playevent', {
          play: true
        })
      })
    },

    toggle () {
      const backgroundAudioManager = wx.getBackgroundAudioManager()
      console.log('按钮了')
      if (this.data.isPlay) {
        console.log('暂停')
        backgroundAudioManager.pause()
        this.triggerEvent('playevent', {
          play: false
        })
      } else {
        console.log('播放')
        backgroundAudioManager.play()
        this.triggerEvent('playevent', {
          play: true
        })
      }
    },

    bindViewTap () {
      wx.navigateTo({
        url: `../../pages/music/index?id=${wx.getStorageSync('currentMusic').id}`
      })
    }
  }
})
