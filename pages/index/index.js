//index.js
//获取应用实例
const app = getApp()
let api = require('../../api/index')

Page({
  data: {
    isShowLoading: true,
    imgUrls: [], // banner
    recommendList: [], // 推荐歌单

    name: '未知歌曲',
    author: '未知作者',
    pic: '',
    play: true,
    isSave: false,
    id: '',

    flag: true,

    userInfo: {},
    hasUserInfo: false,
    canIUse: wx.canIUse('button.open-type.getUserInfo')
  },

  onLoad () {
    this.fetchMusicData()
    console.log('=============================================首页加载=======================================================')
  },

  onReady () {
    console.log('=============================================首页初次渲染完成==================================================')
  },

  onShow () {
    this.fetchMusic()
    console.log('=============================================首页显示===============================================================')
  },

  onUnload () {
    console.log('=======================================================首页卸载=========================================')
  },

  onHide () {
    console.log('==================================================首页隐藏================================================')
  },

  //事件处理函数
  bindViewTap (e) {
    let id = e.currentTarget.dataset.id
    wx.navigateTo({
      url: `../details/index?id=${id}`
    })
  },

  fetchMusic () {
    const backgroundAudioManager = wx.getBackgroundAudioManager()
    console.log(backgroundAudioManager.src)

    if (!backgroundAudioManager.src) return

    // 获取本地存储
    let currentMusic = wx.getStorageSync('currentMusic')
    console.log('当前的播放状态======', !backgroundAudioManager.paused)
    this.setData({
      play: !backgroundAudioManager.paused,
      name: currentMusic.name || this.data.name,
      author: currentMusic.author || this.data.author,
      id: currentMusic.id || this.data.id,
      pic: currentMusic.pic || this.data.pic,
      isSave: true,
      flag: !this.data.flag
    })
  },

  onPlayevent (e) {
    console.log(e.detail)
    this.setData({
      play: e.detail.play
    })
  },

  fetchMusicData () {
    wx.showLoading({
      title: '加载中',
    })
    let promise = [
      api.getBanner(),
      api.personalized()
    ]

    Promise.all(promise).then(res => {
      let [banners, personalist] = res
      // 获取banner
      this.setData({
        imgUrls: banners.data.banners,
        recommendList: personalist.data.result,
        isShowLoading: false
      })
      wx.hideLoading()
    }).catch((err) => {
      wx.showModal({
        title: '提示',
        content: err.errMsg
      })
      this.setData({
        isShowLoading: false
      })
      wx.hideLoading()
    })
  },

  // 获取用户信息
  fetchInfo () {
    if (app.globalData.userInfo) {
      this.setData({
        userInfo: app.globalData.userInfo,
        hasUserInfo: true
      })
    } else if (this.data.canIUse){
      // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
      // 所以此处加入 callback 以防止这种情况
      app.userInfoReadyCallback = res => {
        this.setData({
          userInfo: res.userInfo,
          hasUserInfo: true
        })
      }
    } else {
      // 在没有 open-type=getUserInfo 版本的兼容处理
      wx.getUserInfo({
        success: res => {
          app.globalData.userInfo = res.userInfo
          this.setData({
            userInfo: res.userInfo,
            hasUserInfo: true
          })
        }
      })
    }
  },

  getUserInfo: function(e) {
    app.globalData.userInfo = e.detail.userInfo
    this.setData({
      userInfo: e.detail.userInfo,
      hasUserInfo: true
    })
  }
})
