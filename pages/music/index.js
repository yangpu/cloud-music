let api = require('../../api/index')
Page({

  data: {
    id: '',
    lyricArr: [],
    lyric: '',
    current: '',
    setId: null
  },
  

  onLoad (opts) {
    console.log('====================================详情播放页加载===========================================')
    this.setData({
      id: opts.id
    })
    this.fetchMusicLysic(opts.id)
   
  },


  onReady () {
    console.log('=======================================详情播放页初次渲染完成=====================================')
  },

  onShow () {
    console.log('--------------------------------------------详情播放页显示----------------------------------------')
  },

  onUnload () {
    clearTimeout(this.setId)
    console.log('==========================================详情播放页卸载========================================')
  },

  onHide () {
    console.log('===============================================详情播放页隐藏==========================================')
  },

  fetchMusicLysic (id) {
    api.getMusicLysic(id).then(res => {
      console.log(res.data.lrc.lyric)
      let lyricArr = res.data.lrc.lyric.split('\n')
      console.log(lyricArr)
      let result = []
      for (let item of lyricArr) {
        let start = item.indexOf('[')
        let end = item.indexOf(']')
        if (start > -1 && end > -1) {
          let time = item.slice(start + 1, end)
          let song = item.slice(end + 1)
          let date = time.replace(/:|\./g, ',').split(',')
          let sec = Number(date[0]) * 60 + Number(date[1]) + Number(date[2]) / 100
          result.push({
            time,
            song,
            date,
            sec
          })
        }
      }
      console.log(result)
      this.setData({
        lyric: res.data.lrc.lyric,
        lyricArr: result
      })
      this.handleMusic()
    })
    console.log(wx.getBackgroundAudioManager().currentTime)
    
  },

  handleMusic () {
    let lyricArr = this.data.lyricArr
    let fun = () => {
      let time = wx.getBackgroundAudioManager().currentTime
  
      console.log('歌曲时间', time)
      for (let [key, item] of lyricArr.entries()) {
        console.log(item.sec)
        if (item.sec > time && time < (lyricArr[key] && lyricArr[key].sec)) {
          this.setData({
            current: item.song
          })
          this.setId = setTimeout(fun, 1000)
          break
        }
      }
    }
    fun()
  }
})