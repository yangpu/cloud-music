let api = require('../../api/index')
Page({

  data: {
    details: [],
    isShowLoading: true,
    id: '',
    name: '未知歌曲',
    author: '未知作者',
    pic: '',
    play: true,
    isSave: false
  },
  

  onLoad (opts) {
    console.log('====================================列表播放页加载===========================================')
    this.setData({
      id: opts.id
    })
    this.fetchDetails(opts.id)
    const backgroundAudioManager = wx.getBackgroundAudioManager()

    if (!backgroundAudioManager.src) return
    console.log('当前的播放状态======', !backgroundAudioManager.paused)

    // 获取本地存储
    let currentMusic = wx.getStorageSync('currentMusic')
    this.setData({
      play: !backgroundAudioManager.paused,
      name: currentMusic.name || this.data.name,
      author: currentMusic.author || this.data.author,
      id: currentMusic.id || this.data.id,
      pic: currentMusic.pic || this.data.pic,
      isSave: true
    })
  },


  onReady () {
    console.log('=======================================列表播放页初次渲染完成=====================================')
  },

  onShow () {
    console.log('--------------------------------------------列表播放页显示----------------------------------------')
  },

  onUnload () {
    console.log('==========================================列表播放页卸载========================================')
  },

  onHide () {
    console.log('===============================================列表播放页隐藏==========================================')
  },

  fetchDetails (id) {
    wx.showLoading({
      title: '加载中',
    })
    api.getDetail(id).then(res => {
      this.setData({
        isShowLoading: false,
        details: res.data.result.tracks.map(item => {
          return {
            id: item.id,
            name: item.name,
            pic: item.album.blurPicUrl,
            author: item.artists.length ? item.artists[0].name : '-'
          }
        })
      })
      
      // 保存本地的歌单
      wx.setStorageSync('musicList', this.data.details)

      wx.hideLoading()
    }).catch((err) => {
      wx.showModal({
        title: '提示',
        content: err.errMsg
      })
      this.setData({
        isShowLoading: false
      })
      wx.hideLoading()
    })
  },

  listenToMusic (e) {
    let {id, author, name, pic} = e.currentTarget.dataset
    api.getMusicUrl(id).then(res => {
      if (res.data.data.length) {
        this.setData({
          name,
          author,
          pic,
          play: true,
          isSave: true
        })

        wx.setStorageSync('currentMusic', {
            url: res.data.data[0].url,
            name,
            pic,
            author,
            id,
            isSave: true
        })

        const backgroundAudioManager = wx.getBackgroundAudioManager()

        backgroundAudioManager.title = name
        backgroundAudioManager.epname = name
        backgroundAudioManager.singer = author
        backgroundAudioManager.coverImgUrl = pic
        backgroundAudioManager.src = res.data.data[0].url
      }
      
    })
  },
  onPlayevent (e) {
    console.log(e.detail)
    this.setData({
      play: e.detail.play
    })
  }
})