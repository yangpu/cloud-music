
let {http} = require('./http')

// 首页api
let indexPage = {
  getBanner () {
    return http('banner')
  },

  personalized () {
    return http('personalized')
  }
}

// 详情
let detailsPage = {
  // 获取歌词
  getMusicLysic (id) {
    return http(`lyric?id=${id}`)
  },

  // 获取歌曲地址
  getMusicUrl (id) {
    return http(`music/url?id=${id}`)
  },

  getDetail (id) {
    return http(`playlist/detail?id=${id}`)
  }
}
module.exports = {
  ...indexPage,
  ...detailsPage
}