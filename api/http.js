let API_URL = 'http://120.79.162.149:3000'

function http (type) {
  return new Promise((reslove, reject) => {
    wx.request({
      url: `${API_URL}/${type}`,
      header: {
        'content-type': 'application/json'
      },
      success: reslove,
      fail: reject
    })
  })
}

module.exports = {
  http
}